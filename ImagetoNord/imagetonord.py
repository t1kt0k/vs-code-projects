import os
from ImageGoNord import NordPaletteFile, GoNord
# Get the list of all files and directories
path = "Path To Get Files From" #Example /home/username/Pictures/wallpapers/
newpath = "Path To Output Files To" #Example /home/username/Pictures/nord-wallpapers
dir_list = os.listdir(path)
print (dir_list) # Prints the array of the path variable

go_nord = GoNord()
go_nord.enable_avg_algorithm()
go_nord.reset_palette()
go_nord.add_file_to_palette(NordPaletteFile.POLAR_NIGHT)
go_nord.add_file_to_palette(NordPaletteFile.SNOW_STORM)
go_nord.add_color_to_palette(NordPaletteFile.FROST)
  
print(len(dir_list)) #Prints Index of current file in array
i=0
for f in dir_list:
    
    print(i,f)

    image = go_nord.open_image(path+f)
    go_nord.convert_image(image, save_path=newpath+f)
    i+=1