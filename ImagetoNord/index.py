import os
from ImageGoNord import NordPaletteFile, GoNord
# Get the list of all files and directories
path = "//home//mike//Pictures//wallpapers"
newpath = "//home/mike//Pictures/nord-wallpapers"
dir_list = os.listdir(path)
 
for f in dir_list:
    go_nord.enable_avg_algorithm()
        go_nord.reset_palette()
        go_nord.add_file_to_palette(NordPaletteFile.POLAR_NIGHT)
        go_nord.add_file_to_palette(NordPaletteFile.SNOW_STORM)

        image = go_nord.open_image(f)
        go_nord.convert_image(image, save_path=newpath+f)